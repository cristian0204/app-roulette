﻿using CleanArch.Infra.Data.Context;
using Infra.Ioc;
using Microsoft.Extensions.DependencyInjection;

namespace CleanArch.Infra.Ioc
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<ApplicationDBContext>();
            ApplicationDependencycontainer.RegisterServices(services);
            InfraDependencycontainer.RegisterServices(services);
            CoreServiceContainer.RegisterServices(services);

        }
    }
}
