﻿using Core.Models.Common;
using Domain.Models.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Bet
{
    public class Bet: Entity
    {
        public Guid UserId { get; set; }
        public Guid RouletteId { get; set; }
        public Roulette Roulette { get; set; } = null!;
        public int? BetColor { get; set; }
        public int? BetNumber { get; set; }
        public int? BetAmount { get; set; }
    }
}
