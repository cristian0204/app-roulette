﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Product
{
    public class Roulette : Entity
    {
        public int? Status { get; set; }
        public DateTime? OpenedAt { get; set; }
        public DateTime? ClosedAt { get; set; }
        public Guid? OpenedBy { get; set; }
        public Guid? ClosedBy { get; set; }
        public int? RulerId { get; set; }
        public Ruler Ruler { get; set; }
        public int? WinningNumber { get; set; }
    }
}
