﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Product
{
    public class Ruler : EntityWithIntId
    {
        public int MinimumNumberAllowed { get; set; }
        public int MaximunNumberAllowed { get; set; }
        public int NumberPaymentPercentage { get; set; }
        public int ColourPaymentPercentage { get; set; }
    }
}
