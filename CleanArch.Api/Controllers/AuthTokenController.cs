﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Auth.Commands;
using Application.ViewModel.Auth;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Masiv.Api.Controllers
{
    [Route("api/auth-token")]
    [ApiController]
    public class AuthTokenController : ApiControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> Authentication([FromBody] PostLoginCommand command)
        {

            return Ok(await Mediator.Send(command));
        }
    }
}
